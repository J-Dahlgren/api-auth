import Express from "express";
import * as factory from "../src/index";
const app: Express.Application = Express();
import passport from "passport";
export const TestSecret = "passport_secret"

app.use(passport.initialize());
app.use(passport.session());
let strategy = factory.GetJwtStrategy(TestSecret);
passport.use(strategy);
app.get("/protected", passport.authenticate("jwt", { session: false }), (req, res) => {
    return res.status(200).end();
});
app.get("/norole", passport.authenticate("jwt", { session: false }), factory.CheckRoles(), (req, res) => {
    return res.status(200).end();
});
app.get("/read", passport.authenticate("jwt", { session: false }), factory.CheckRoles(["read"]), (req, res) => {
    return res.status(200).end();
});
app.get("/write", passport.authenticate("jwt", { session: false }), factory.CheckRoles(["write"]), (req, res) => {
    return res.status(200).end();
});

export const App: Express.Application = app;