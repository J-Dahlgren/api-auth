import * as request from "supertest";
import * as test_app from "../mock/test-app";
import * as jwt from "jsonwebtoken";
const NO_ROLES = {};
const EMPTY_ROLES = { roles: [] };
const HAS_ROLES = { roles: ["read"] };
const HAS_ALL_ROLES = { roles: ["read", "write", "junk"] };
const TOKEN_NO_ROLES = `jwt ${jwt.sign(NO_ROLES, test_app.TestSecret)}`;
const TOKEN_EMPTY_ROLES = `jwt ${jwt.sign(EMPTY_ROLES, test_app.TestSecret)}`;
const TOKEN_WITH_ROLES = `jwt ${jwt.sign(HAS_ROLES, test_app.TestSecret)}`;
const TOKEN_WITH_ALL_ROLES = `jwt ${jwt.sign(HAS_ALL_ROLES, test_app.TestSecret)}`;


describe("Endpoint Protection", () => {
    it("Returns 401 on no auth header", (done) => {
        return request.agent(test_app.App)
            .get("/protected")
            .expect(401)
            .then(req => done());
    });
    it("Returns 200 on valid token", (done) => {
        return request.agent(test_app.App)
            .get("/protected")
            .set("Authorization", TOKEN_EMPTY_ROLES)
            .expect(200)
            .then(req => done());
    });
    it("Returns 200 when no roles required", (done) => {
        return request.agent(test_app.App)
            .get("/norole")
            .set("Authorization", TOKEN_NO_ROLES)
            .expect(200)
            .then(req => done());
    });
    it("Returns 403 when user has no roles at all", (done) => {
        return request.agent(test_app.App)
            .get("/read")
            .set("Authorization", TOKEN_NO_ROLES)
            .expect(403)
            .then(req => done());
    });
    it("Returns 200 when user has correct roles", (done) => {
        return request.agent(test_app.App)
            .get("/read")
            .set("Authorization", TOKEN_WITH_ROLES)
            .expect(200)
            .then(req => done());
    });
    it("Returns 403 when use has correct roles", (done) => {
        return request.agent(test_app.App)
            .get("/write")
            .set("Authorization", TOKEN_WITH_ROLES)
            .expect(403)
            .then(req => done());
    });
    it("Returns 200 when use has all correct roles", (done) => {
        return request.agent(test_app.App)
            .get("/write")
            .set("Authorization", TOKEN_WITH_ALL_ROLES)
            .expect(200)
            .then(req => done());
    });
});