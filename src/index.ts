import passport_jwt from "passport-jwt";
import { Request, Response, NextFunction } from "express";
const JwtStrategy = passport_jwt.Strategy;
export interface User {
    roles?: string[];
}
export function GetJwtStrategy(secret: string) {
    return new JwtStrategy(
        {
            jwtFromRequest: passport_jwt.ExtractJwt.fromAuthHeaderWithScheme("jwt"),
            secretOrKey: secret
        },
        (payload, done) => {
            done(null, payload, null);
        }
    );
}
export function CheckRoles<T extends User = User>(roles?: string[]) {
    return function (req: Request, res: Response, next: NextFunction) {
        let user = <T>req.user;
        if (!roles) {
            return next();
        }
        if ((roles.length && (!user || !user.roles))) {
            return res.status(403).send('forbidden');
        }
        for (let role of roles) {
            if (!user.roles || user.roles.indexOf(role) < 0) {
                return res.status(403).send('forbidden');
            }
        }
        next();
    }
}
